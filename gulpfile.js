/*=================================
=            Variables            =
=================================*/

const gulp = require('gulp'),
gutil = require('gulp-util'),
sass = require('gulp-sass'),
uglify = require('gulp-uglify'),
autoprefixer = require('autoprefixer'),
browserSync = require('browser-sync'),
cssnano = require('gulp-cssnano'),
sourcemaps = require('gulp-sourcemaps'),
notify = require("gulp-notify"),
imagemin = require("gulp-imagemin"),
imageminMozjpeg = require('imagemin-mozjpeg'),
bro = require('gulp-bro'),
babelify = require('babelify'),
package = require('./package.json'),
paths = {
    nodeModules: 'node_modules/',
    polyfill: 'node_modules/@babel/polyfill/dist/polyfill.min.js',
    assets: {
      css: './assets/css',
      js: './assets/js',
      img: './assets/images',
    }
};

var banner = [
  '/*!\n' +
  ' * <%= package.name %>\n' +
  ' * <%= package.title %>\n' +
  ' * @author <%= package.author %>\n' +
  ' * @version <%= package.version %>\n' +
  ' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
  ' */',
  '\n'
].join('');

/*==========================================
=        Development tasks [watcher]       =
==========================================*/

/* gulp.task('css') = Watcher for development */
gulp.task('css', function () {
    return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
        includePaths: [].concat(
          paths.nodeModules, /* paths.nodeModules allows us to use imports in scss coming from node modules*/
          require('node-bourbon').includePaths
        )
    })
      .on("error", notify.onError({
        message: "<%= error.message %>",
        title: "SCSS ERROR"
      })))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.assets.css))
    .pipe(browserSync.reload({stream:true}));
});

/* gulp.task('js') = Watcher for development */
gulp.task('js', function() {
    gulp.src(['src/js/**/*.js'])
        .pipe(bro({
          transform: [
            babelify.configure({
              presets: ['@babel/env'],
              sourceMaps: true
            })
          ]
        }))
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(paths.assets.js))
        .pipe(browserSync.reload({stream:true}));
});

/*========================================
=        Production tasks [build]        =
========================================*/

/* gulp.task('css-min') = Build for production [no sourcemaps/banners] */
gulp.task('css-min', function () {
    return gulp.src('src/scss/**/*.scss')
    .pipe(sass({
                includePaths: [].concat(
                  paths.nodeModules, /* paths.nodeModules allows us to use imports in scss coming from node modules*/
                  require('node-bourbon').includePaths
                )
            }).on("error", notify.onError({
        message: "<%= error.message %>",
        title: "SCSS ERROR"
      })))
    .pipe(cssnano())
    .pipe(gulp.dest(paths.assets.css))
    .pipe(header(banner, { package : package }))
});

/* gulp.task('js-min') = Build for production [no sourcemaps/banners] */
gulp.task('js-min',function(){
    gulp.src(['src/js/**/*.js'])
        .pipe(bro({
          transform: [
            babelify.configure({ presets: ['@babel/env'] })
          ]
        }))
        .pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(paths.assets.js))
        .pipe(header(banner, { package : package }))
});

/* gulp.task('img-min') = Build for production [compress jpg by 40%, png 50%] */
gulp.task('img-min', () =>
    gulp.src('src/images/**/*')
        .pipe(imagemin([
            imageminMozjpeg({
                quality: 60
            }),
            imagemin.optipng({ optimizationLevel: 5 })
        ]))
        .pipe(gulp.dest(paths.assets.img))
);

/*=============================================
=                Gulp Commands               =
=============================================*/

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        server: {
            baseDir: "./"
        }
    });
});
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('watch', ['css', 'js', 'browser-sync'], function () {
    gulp.watch("src/scss/**/*.scss", ['css']);
    gulp.watch("src/js/**/*.js", ['js']);
    gulp.watch("./*.html", ['bs-reload']);
});

gulp.task('build', ['css-min', 'js-min', 'img-min']);